#
# FETCH DEPENDENCIES...
#

FROM composer:1.8 as composer
WORKDIR /src
COPY . /src
RUN composer install \
    --ignore-platform-reqs \
    --no-scripts

#
# Compile js & css assets
#

FROM node:10.15-alpine as node
WORKDIR /node
COPY --from=composer /src /node
RUN npm install --silent && \
    npm run dev --silent
RUN rm -rf /node/node_modules

#
# AS APP
#

FROM php:7.1-apache-stretch

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            libpng-dev libpq-dev libssl1.0-dev \
            libxrender-dev libfontconfig1-dev \
            libxext-dev libicu-dev && \
    pecl install -f xdebug && \
    rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql && \
    docker-php-ext-enable xdebug && \
    docker-php-ext-install pdo pdo_pgsql pdo_mysql intl mbstring gd exif zip

COPY --from=node /node /var/www/html/

# Change ownership
RUN mkdir -p cache public/uploads && \
    chgrp -R www-data bootstrap/cache \
                      cache \
                      public/uploads \
                      storage && \
    chmod -R 775 bootstrap/cache \
                 cache \
                 public/uploads \
                 storage

# Change Document Root
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' \
                /etc/apache2/sites-available/*.conf && \
    a2enmod rewrite
