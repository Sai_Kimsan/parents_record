.DEFAULT_GOAL:=help
SHELL:=/bin/bash

##@ Building

.PHONY: install reinstall

install:  ## Build docker image and start containers
	@bash -e "scripts/docker/build-docker.sh"

reinstall: ## Rebuild docker image and start containers
	@docker-compose up -d --build

##@ Containers

.PHONY: up down list

up: ## Start containers
	@docker-compose up -d

down: ## Shutdown and remove containers
	@docker-compose down

list: ## Listing all running containers
	@docker ps

##@ Clean
.PHONY: delete clean

clean: ## Clean containers, volumes, networks in docker-compose
	@docker-compose down -v

delete: ## Clean all docker images
	@docker system prune -a

##@ Helpers

.PHONY: help

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
