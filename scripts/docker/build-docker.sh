# INFO Using insecure 'fake' key because 'key:generate'
# does not add it to '.env.testing' (only .env) which fails the tests


### Create .env for docker-compose
{
echo "APP_ENV=local"
echo "DB_CONNECTION=pgsql"
echo "DB_HOST=db"
echo "DB_PORT=5432"
echo "DB_USERNAME=parents"
echo "DB_PASSWORD=parents"
echo "DB_DATABASE=parents_db"
} > .env
# INFO Using insecure 'fake' key because 'key:generate'
# does not add it to '.env.testing' (only .env) which fails the tests

### Start docker-composer
docker-compose up -d || continue

echo "- Sleep for 10 seconds..."
sleep 10 || continue

### Import APP_KEY for .env and run migration seed db
echo "Import APP_KEY for .env and run migrate seed for DB"
echo "APP_KEY=base64:leuy/NskJTFIdw/0IMgX7s8POH0mEkRHD1EBfnS5REM=" >> .env
docker-compose exec app php artisan migrate:fresh --seed --force --no-interaction

echo "- Clear caches..."
docker-compose exec app php artisan cache:clear
docker-compose exec app php artisan config:clear
docker-compose exec app php artisan route:clear

echo "Finish for initial Parents using via Docker"

## Access student service by browser
#/usr/bin/env bash
URL=http://localhost:8000

if which xdg-open > /dev/null; then
  xdg-open $URL
elif which gnome-open > /dev/null; then
  gnome-open $URL
else
  echo "Could not detect the browser to use."
fi
